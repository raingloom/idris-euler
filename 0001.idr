module Main

%default total

{-
win idris --nocolor $%
idris -o $%.out $% && $%.out
-}

multiples_of : Nat -> Stream Nat
multiples_of n = f n 0 where
	f : Nat -> Nat -> Stream Nat
	f c x = x :: f c (x + c)

positive_multiples_of : Nat -> Stream Nat
positive_multiples_of n = drop 1 $ multiples_of n

skip_each_nth : (n0, n : Nat) -> {auto prf : LTE 2 n}-> Stream a -> Stream a
skip_each_nth n0 n {prf} s = let prf' = prf in
	let n' = Nat.(-) n {smaller = lteSuccLeft prf} 1 in f n0 n' s where
		f Z c (_::x::xs) = x :: f (pred c) c xs
		f (S m) c (x::xs) = x :: f m c xs

limited_sum_while : Nat -> (Nat -> Bool) -> Stream Nat -> Nat
limited_sum_while lim g xs = f lim 0 xs where
	f : Nat -> Nat -> Stream Nat -> Nat
	f Z sum _ = sum
	f (S k) sum (x::xs) = if g x then f k (sum + x) xs else sum

sumPrimeMultiples : Nat -> (p0, p1 : Nat) -> {auto prf0 : LTE 2 p0} -> {auto prf1 : LTE 2 p1} -> Nat
sumPrimeMultiples lim p0 p1 = s' lim p0 p1 + s' lim p1 p0 + (limited_sum_while lim ltLim $ multiples_of (5 * 3)) where
	ltLim : Nat -> Bool
	ltLim n = n < lim
	s' : Nat -> (m,n : Nat) -> {auto prf : LTE 2 n} -> Nat
	s' lim m n {prf} = limited_sum_while lim ltLim (skip_each_nth 0 n {prf=prf} $ multiples_of m)

small_test : sumPrimeMultiples 10 3 5 = 23
small_test = Refl

{-
-- probably correct but it takes too long to compile, so just run one of the codegens instead and check the output of that
solution : sumPrimeMultiples 1000 3 5 = 233168
solution = Refl
-}

main : IO ()
main = do
	putStrLn $ show $ sumPrimeMultiples 1000 3 5
